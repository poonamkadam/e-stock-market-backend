module.exports = app => {
  const marketController = require("../controllers/market.controller.js");
  const marketValidator = require('../helper/validation.helper');

  var router = require("express").Router();

  // Fetches all the Company Details
  router.get('/company/getall', marketController.findAll);

  // Register a new company
  router.post('/company/register',marketValidator.validate('createCompany'), marketController.createCompany);

  //Fetches the Company Details
  router.get('/company/info/:code', marketController.findCompanyByCode);

  // Deletes a company
  router.delete('/company/delete/:code', marketController.deleteCompanyByCode);

  // Add new stock price
  router.post('/stock/add/:code',marketValidator.validate('addStock'), marketController.createStock);

  // Fetches Stock Price List
  router.get('/stock/get/:code/:startdate/:enddate', marketController.findStocksByCode);


  app.use('/api/v1.0/market', router);
};