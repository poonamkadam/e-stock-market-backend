'use strict';

const {body} = require('express-validator');

const requiredValidationMsg = (reqValue)=>{
    return reqValue+" is missing"; 
}

exports.validate = (method) => {
  switch (method) {
    case 'createCompany': {
     return [ 
        body('company_code',requiredValidationMsg('company_code')).exists(),
        body('company_name',requiredValidationMsg('company_name')).exists(),
        body('company_ceo',requiredValidationMsg('company_ceo')).exists(),
        body('turnover',requiredValidationMsg('turnover')).exists(),
        body('website',requiredValidationMsg('website')).exists(),
        body('stock_exchange',requiredValidationMsg('stock_exchange')).exists()
       ]   
    }
    case 'addStock': {
      return [ 
         body('companyId',requiredValidationMsg('companyId')).exists(),
         body('stock_price', requiredValidationMsg('stock_price')).exists(),
        ]   
     }
  }compnayId
};