module.exports = {
  HOST: "stockapp.cptngb4egtft.us-east-2.rds.amazonaws.com",
  USER: "root",
  PASSWORD: "password",
  DB: "e_stock_market",
  dialect: "mysql",
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  }
};
