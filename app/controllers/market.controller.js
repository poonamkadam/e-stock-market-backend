'use strict';

const db = require("../models");
const {validationResult } = require('express-validator');
const Company = db.company;
const Stock = db.stocks;
const maxTurnoverValue = '100000000';
const sequelize = require('sequelize')
const Op = sequelize.Op
// Get all company include stocks
exports.findAll = (req,res) => {
  Company.findAll({
     include: ["stocks"],
   })
   .then(data => {
     console.log(">> Created Company: " + JSON.stringify(data, null, 4));
     res.send({status:200,error:false,data:data});

   })
   .catch(err => {
     res.status(500).send({
       message:
         err.message || "Some error occurred while creating the Company."
     });
   });
 };
 
// Create and Save new company
exports.createCompany = (req,res) => {
   // Validate incoming input
     // Finds the validation errors in this request and wraps them in an object with handy functions
     const errors = validationResult(req); 

     if (!errors.isEmpty()) {
       res.status(422).json({ status:422, error:true,errors: errors.array() });
       return;
     }
       //handles turnover invalid value error 
    if(parseInt(req.body.turnover) < maxTurnoverValue){
      res.status(400).send({status:400 , error:true, message: 'Company Turnover should be more 10Cr' });
      return;
    }
   Company.create({
    company_code: req.body.company_code,
    company_name: req.body.company_name,
    company_ceo: req.body.company_ceo,
    turnover: parseInt(req.body.turnover), 
    website: req.body.website, 
    stock_exchange: req.body.stock_exchange,
  }).then(data => {
    console.log(">> Created Company: " + JSON.stringify(data, null, 4));
    res.send({status:200,error:false,message:"Company register succesfully",data:data});
  })
  .catch(err => {
      res.status(500).send({
        status:422,
        error:true,
        message:
          err.message || "Some error occurred while creating the Company."
      });
  });
};

// Get the stocks for a given Company
exports.findCompanyByCode = (req,res) => {
  return Company.findOne({where:{ company_code: req.params.code} ,include: ["stocks"]} )
    .then((data) => {
      res.send({status:200,error:false,data:data});

    })
    .catch(err => {
      res.status(500).send({
        status:422,
        error:true,
        message:
          err.message || "Some error occurred while creating the Company."
      });
  });
};


// Create and Save new stocksss
exports.createStock = (req,res) => {
   // Validate incoming input
     // Finds the validation errors in this request and wraps them in an object with handy functions
     const errors = validationResult(req); 

     if (!errors.isEmpty()) {
       res.status(422).json({ status:422, error:true,errors: errors.array() });
       return;
     }
  return Stock.create({
    stock_price: req.body.stock_price,
    companyId: parseInt(req.body.companyId),
    stock_created_date: new Date(),
    company_code: req.params.code,
  })
    .then((stock) => {
      console.log(">> Created Stock: " + JSON.stringify(stock, null, 4));
      res.send({status:200,error:false,data:stock});
    })
    .catch(err => {
      res.status(500).send({
        status:422,
        error:true,
        message:
          err.message || "Some error occurred while creating the Company."
      });
  });
};



// Get the stocks for a given Company
exports.findStocksByCode = (req,res) => {
  return Stock.findAll({ where: { stock_created_date: { [Op.gt]:req.params.startdate,[Op.lt]:req.params.enddate},company_code: req.params.code} }, { include: ["company"] })
    .then((data) => {
      res.send({status:200,error:false,data:data});
    
    })
    .catch(err => {
      res.status(500).send({
        status:422,
        error:true,
        message:
          err.message || "Some error occurred while creating the Company."
      });
  });
};

// Get the stocks for a given Stock id
exports.findStockById = (id) => {
  return Stock.findByPk(id, { include: ["Company"] })
    .then((Stock) => {
      return Stock;
    })
    .catch(err => {
      res.status(500).send({
        status:422,
        error:true,
        message:
          err.message || "Some error occurred while creating the Company."
      });
  });
};



// Delete a Company with the specified id in the request
exports.deleteCompanyByCode = (req, res) => {
    return Company.findOne({ where:{ company_code: req.params.code} }, { include: ["stocks"] })
    .then((data) => {
      if(data){
      Stock.destroy({where:{ company_code: req.params.code}})
      Company.destroy({where:{ company_code: req.params.code}})
      res.send({status:200,error:false,message: "Company was deleted successfully!"});
      }else{
        res.send({
          message: `Cannot delete Company with code=${req.params.code}. Maybe Company was not found!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        status:422,
        error:true,
        message:
          err.message || "Some error occurred while creating the Company."
      });
  });
}
