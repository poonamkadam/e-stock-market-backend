
module.exports = (sequelize, Sequelize) => {
  const Company = sequelize.define("companies", {
    company_code: {
      type: Sequelize.STRING,
      allowNull: {
        args: false,
        msg: 'company_code is required.'
      },
      unique: {
        args: true,
        msg: 'company_code should be unique, this code alredy exit'
      },
    },
    company_name: {
      type: Sequelize.STRING,
      allowNull: {
        args: false,
        msg: 'company_name is required.'
      },
    },
    company_ceo: {
      type: Sequelize.STRING,
      allowNull: {
        args: false,
        msg: 'company_ceo is required.'
      }
    },
    turnover: {
      type: Sequelize.BIGINT,
      allowNull: {
        args: false,
        msg: 'turnover is required.'
      }
    },
    website: {
      type: Sequelize.STRING,
      allowNull: {
        args: false,
        msg: 'website is required.'
      }
    },
    stock_exchange: {
      type: Sequelize.STRING,
      allowNull: {
        args: false,
        msg: 'stock_exchange is required.'
      }
    }
  });

  return Company;
};
