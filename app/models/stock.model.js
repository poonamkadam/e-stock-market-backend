
module.exports = (sequelize, Sequelize) => {
  const Stock = sequelize.define("stocks", {
    stock_price: {
      type: Sequelize.DECIMAL(35,2),
      allowNull: false
    },
    stock_created_date: {
      type: Sequelize.DATE,
      allowNull: false
    },
    company_code: {
      type: Sequelize.STRING,
      allowNull: {
        args: false,
        msg: 'company_code is required.'
      }
    },
  
  });

  return Stock;
};
